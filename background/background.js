"use strict";

const main = () => {
	const CONTEXT_ID = "downloadImage";

	const onInstalled = details => {
		debug("create context menu");

		chrome.contextMenus.create({
			type: "normal",
			id: CONTEXT_ID,
			title: "Download image",
			contexts: ["image"]
		});
	};

	const onClicked = (info, tab) => {
		debug("context menu clicked:", info, tab);

		if (info.menuItemId === CONTEXT_ID) {
			debug("request download of", info.srcUrl);

			const options = {
				url: info.srcUrl,
				conflictAction: "uniquify"
			};

			chrome.downloads.download(options, downloadId => {
				chrome.storage.local.get({
					downloads: {}
				}, data => {
					data.downloads["dl" + downloadId] = {
						tab: tab.id
					};
					chrome.storage.local.set(data);
				});
			});
		}
	};

	const onMessage = function(msg, sender, sendResponse) {
		if (msg.subject === "downloadRequested") {
			debug("download of", msg.url, "requested");
			const options = {
				url: msg.url,
				conflictAction: "uniquify"
			};

			chrome.downloads.download(options, downloadId => {
				debug("sending download id", downloadId, "to tab");
				sendResponse({
					id: downloadId
				});

				chrome.storage.local.get({
					downloads: {}
				}, data => {
					data.downloads["dl" + downloadId] = {
						tab: sender.tab.id
					};
					chrome.storage.local.set(data);
				});
			});

			return true;
		}
	};
	
	const onDeterminingFilename = (downloadItem, suggest) => {
		chrome.storage.sync.get({
			enableRename: false,
			fileNamePattern: "%original%",
			counterPadding: 1,
			enableSubfolder: false,
			subfolderNamePattern: "%imagedomain%"
		}, options => {
			chrome.storage.local.get({
				downloads: {},
				downloadCounter: 1
			}, data => {
				if (options.enableRename) {
					chrome.storage.local.set({
						downloadCounter: data.downloadCounter + 1
					});
				}

				const tab = data.downloads["dl" + downloadItem.id];
				if (!tab) {
					debug("download", downloadItem.id, "seems external - ignoring");
					suggest();
				}
				const tabId = tab.tab;
				
				chrome.tabs.get(tabId, tab => {
					if (options.enableRename || options.enableSubfolder) {
						const file = {
							name: "",
							extension: ""
						};
						const periodIndex = downloadItem.filename.lastIndexOf(".");
						
						if (~periodIndex) {
							file.name = downloadItem.filename.substring(0, periodIndex);
							file.extension = downloadItem.filename.substring(periodIndex);
						} else {
							file.name = downloadItem.filename;
						}
						
						const imageUrl = new URL(downloadItem.url);
						const tabUrl = new URL(tab.url);
						
						if (options.enableRename) {
							file.name = options.fileNamePattern
								.replace(/%original%/g, file.name)
								.replace(/%pagedomain%/g, tabUrl.hostname)
								.replace(/%imagedomain%/g, imageUrl.hostname)
								.replace(/%title%/g, tab.title)
								.replace(/%counter%/g, (() => {
									let out = String(data.downloadCounter);
									return "0".repeat(Math.max(0, options.counterPadding - out.length)) + out;
								})())
							;
						}
						if (options.enableSubfolder) {
							file.name = options.subfolderNamePattern
								.replace(/%pagedomain%/g, tabUrl.hostname)
								.replace(/%imagedomain%/g, imageUrl.hostname)
								.replace(/%title%/g, tab.title)
							+ "/" + file.name;
						}
						
						debug("suggesting filename", file.name, file.extension, "for download", downloadItem.id);
						suggest({
							filename: file.name + file.extension,
							conflictAction: "uniquify"
						});
					} else {
						suggest();
					}
				});
			});
		});
		
		return true;
	};
	
	const onChangedDownloadFound = downloadItems => {
		const downloadItem = downloadItems[0];
		chrome.notifications.create({
			type: "image",
			title: "Image Downloaded",
			message: downloadItem.filename,
			iconUrl: "/images/icon-128.png",
			imageUrl: downloadItem.filename,
			buttons: [{
				title: "View image"
			}, {
				title: "Open folder"
			}]
		}, notificationId => {
			chrome.storage.local.get({
				notifications: {}
			}, data => {
				data.notifications[notificationId] = {
					download: downloadItem.id
				};
				chrome.storage.local.set(data);
			});
		});
	};
	
	const onDownloadChanged = downloadDelta => {
		chrome.storage.sync.get({
			notify: true,
		}, options => {
			if (downloadDelta.state !== undefined && downloadDelta.state.current === "complete") {
				debug("download", downloadDelta.id, "completed");
				chrome.storage.local.get({
					downloads: {}
				}, data => {
					const tab = data.downloads["dl" + downloadDelta.id];
					if (!tab) {
						debug("download", downloadDelta.id, "seems external - ignoring");
						return;
					}
					const tabId = tab.tab;

					delete data.downloads["dl" + downloadDelta.id];
					chrome.storage.local.set(data);

					debug("sending download id", downloadDelta.id, "finished message to tab");
					chrome.tabs.sendMessage(tabId, {
						subject: "downloadFinished",
						id: downloadDelta.id
					});
				});

				if (options.notify === true) {
					chrome.downloads.search({
						id: downloadDelta.id
					}, onChangedDownloadFound);
				}
			}
		});
	};
	
	const onButtonClicked = (notificationId, buttonIndex) => {
		debug("notification", notificationId, "clicked");
		chrome.storage.local.get({
			notifications: {}
		}, data => {
			const id = data.notifications[notificationId].download;
			switch (buttonIndex) {
				case 0:
					chrome.downloads.open(id);
					break;
				
				case 1:
					chrome.downloads.show(id);
					break;
			}
		});
	};
	
	const onNotificationClosed = (notificationId) => {
		debug("notification", notificationId, "closed");
		chrome.storage.local.get({
			notifications: {}
		}, data => {
			delete data.notifications[notificationId];
			chrome.storage.local.set(data);
		});
	};
	
	chrome.runtime.onMessage.addListener(onMessage);
	chrome.downloads.onDeterminingFilename.addListener(onDeterminingFilename);
	chrome.downloads.onChanged.addListener(onDownloadChanged);
	chrome.notifications.onButtonClicked.addListener(onButtonClicked);
	chrome.notifications.onClosed.addListener(onNotificationClosed);
	chrome.runtime.onInstalled.addListener(onInstalled);
	chrome.contextMenus.onClicked.addListener(onClicked);
};

chrome.runtime.onStartup.addListener(() => {
	chrome.storage.local.set({
		downloads: {},
		notifications: {},
		downloadCounter: 1
	});
});