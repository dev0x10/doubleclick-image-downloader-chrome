"use strict";

(() => {
	chrome.storage.sync.get({
		notify: true,
		oneClick: true,
		persist: true,
		requireShift: false,
		minimumImageSize: 100,
		excludedPageDomains: [],
		excludedSourceDomains: [],
		singleClickEnabled: false,
		buttonSize: 48,
		buttonOpacity: 1,
		buttonPosition: "2_3",
		enableRename: false,
		fileNamePattern: "%original%",
		enableSubfolder: false,
		subfolderNamePattern: "%imagedomain%"
	}, options => {
		const overwrite = {};
		
		//
		
		const domainNameRegex = /^([\w\-]+\.)+[\w\-]+$/i;
		const multiWhitespaceRegex = /\s{2,}/g;
		const nonWhitespaceRegex = /\S/;
		
		if (typeof(options.notify) !== "boolean") {
			overwrite.notify = false;
		}

		if (typeof(options.oneClick) !== "boolean") {
			overwrite.oneClick = false;
		}

		if (typeof(options.persist) !== "boolean") {
			overwrite.persist = true;
		}
		
		if (typeof(options.requireShift) !== "boolean") {
			overwrite.requireShift = false;
		}
		
		if (typeof(options.minimumImageSize) !== "number" || options.minimumImageSize < 0 || options.minimumImageSize%1 !== 0) {
			overwrite.minimumImageSize = 100;
		}
		
		if (!Array.isArray(options.excludedPageDomains)) {
			overwrite.excludedPageDomains = [];
		} else {
			const filtered = options.excludedPageDomains.filter(line => domainNameRegex.test(line));
			if (filtered.length !== options.excludedPageDomains.length) {
				overwrite.excludedPageDomains = filtered;
			}
		}
		
		if (!Array.isArray(options.excludedSourceDomains)) {
			overwrite.excludedSourceDomains = [];
		} else {
			const filtered = options.excludedSourceDomains.filter(line => domainNameRegex.test(line));
			if (filtered.length !== options.excludedSourceDomains.length) {
				overwrite.excludedSourceDomains = filtered;
			}
		}
		
		if (typeof(options.singleClickEnabled) !== "boolean") {
			overwrite.singleClickEnabled = false;
		}
		
		if (typeof(options.buttonSize) !== "number" || options.buttonSize < 8 || options.buttonSize > 128 || options.buttonSize%1 !== 0) {
			overwrite.buttonSize = 48;
		}
		
		if (typeof(options.buttonOpacity) !== "number" || options.buttonOpacity < 0.1 || options.buttonOpacity > 1 || (options.buttonOpacity)*100%1 !== 0) {
			overwrite.buttonOpacity = 1;
		}
		
		const buttonPositions = [
			"1_2", "1", "2_1",
			"1_3", "1_4", "2", "2_3", "2_4",
			"3", "4", "5", "6", "7",
			"3_1", "3_2", "8", "4_1", "4_2",
			"3_4", "9", "4_3"
		];
		if (typeof(options.buttonPosition) !== "string" || !~buttonPositions.indexOf(options.buttonPosition)) {
			overwrite.buttonPosition = "2_3";
		}
		
		if (typeof(options.enableRename) !== "boolean") {
			overwrite.enableRename = false;
		}
		
		if (typeof(options.fileNamePattern) !== "string" || (options.enableRename && !nonWhitespaceRegex.test(options.fileNamePattern))) {
			overwrite.enableRename = false;
			overwrite.fileNamePattern = "%original%";
		} else {
			if (multiWhitespaceRegex.test(options.fileNamePattern)) {
				overwrite.fileNamePattern = options.fileNamePattern.replace(multiWhitespaceRegex, " ");
			}
		}
		
		if (typeof(options.enableSubfolder) !== "boolean") {
			overwrite.enableSubfolder = false;
		}
		
		if (typeof(options.subfolderNamePattern) !== "string" || (options.enableSubfolder && !nonWhitespaceRegex.test(options.subfolderNamePattern))) {
			overwrite.enableRename = false;
			overwrite.subfolderNamePattern = "%imagedomain%";
		} else {
			if (multiWhitespaceRegex.test(options.subfolderNamePattern)) {
				overwrite.subfolderNamePattern = options.subfolderNamePattern.replace(multiWhitespaceRegex, " ");
			}
		}
		
		//
		
		const keys = Object.keys(overwrite);
		if (keys.length > 0) {
			debug("resetting", overwrite);

			chrome.storage.sync.set(overwrite, chrome.runtime.openOptionsPage);
		}

		main();
	});
})();